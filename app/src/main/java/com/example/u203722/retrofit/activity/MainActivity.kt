package com.example.u203722.retrofit.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.example.u203722.retrofit.R
import com.example.u203722.retrofit.model.MoviesResponse
import com.example.u203722.retrofit.rest.ApiInterface
import com.example.u203722.retrofit.rest.ApiClient
import android.widget.Toast
import com.example.u203722.retrofit.adapter.MoviesAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.simpleName
    private val API_KEY = "6908d2ecfcdd60607240f3e5afda7dee"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * checks if the api key is empty
         * if it is empty then a toast is shown to the user and its returned
         */
        if (API_KEY.isEmpty()) {
            Toast.makeText(applicationContext, "Please obtain your API KEY first from themoviedb.org", Toast.LENGTH_LONG).show()
            return
        }

        /**
         * finds recycler view and then takes the layout
         * manager of this sets a linear layout manager to it
         */
        val recyclerView = findViewById<RecyclerView>(R.id.movies_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)

        /**
         * I don't like the force unwrapping with the !! but this client should never be null with the check that is added
         */
        val apiService = ApiClient.client!!.create(ApiInterface::class.java)

        /**
         * this calls the top rated movies api
         * when the data is returned we can work with the said data in the enqueue func
         */
        val call = apiService.getTopRatedMovies(API_KEY)
        call.enqueue(object : Callback<MoviesResponse> {
            override fun onResponse(call: Call<MoviesResponse>, response: Response<MoviesResponse>) {
                val statusCode = response.code()
                val movies = response.body().results
                recyclerView.adapter = MoviesAdapter(movies, R.layout.list_item_movie, applicationContext)
                Log.d("main", "Number of movies received: " + movies.size)
            }

            override fun onFailure(call: Call<MoviesResponse>, t: Throwable) {
                // Log error here since request failed
                Log.e("main", t.toString())
            }
        })
    }
}
