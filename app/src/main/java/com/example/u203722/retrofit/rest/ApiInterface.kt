package com.example.u203722.retrofit.rest

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import com.example.u203722.retrofit.model.MoviesResponse


/**
 * Created by u203722 on 20/11/17.
 */
interface ApiInterface {
    @GET("movie/top_rated")
    fun getTopRatedMovies(@Query("api_key") apiKey: String): Call<MoviesResponse>

    @GET("movie/{id}")
    fun getMovieDetails(@Path("id")id: Int, @Query("api_key") apiKey: String): Call<MoviesResponse>
}