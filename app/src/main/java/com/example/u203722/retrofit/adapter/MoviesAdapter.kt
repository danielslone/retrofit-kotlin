package com.example.u203722.retrofit.adapter

/**
 * Created by u203722 on 20/11/17.
 */
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.u203722.retrofit.R
import com.example.u203722.retrofit.model.Movie

class MoviesAdapter(private val movies: List<Movie>, private val rowLayout: Int, private val context: Context) : RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>() {

    class MovieViewHolder(v: View) : RecyclerView.ViewHolder(v),  View.OnClickListener {
        internal var moviesLayout: LinearLayout
        internal var movieTitle: TextView
        internal var data: TextView
        internal var movieDescription: TextView
        internal var rating: TextView


        init {
            moviesLayout = v.findViewById(R.id.movies_layout)
            movieTitle = v.findViewById(R.id.title)
            data = v.findViewById(R.id.subtitle)
            movieDescription = v.findViewById(R.id.description)
            rating = v.findViewById(R.id.rating)
            v.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesAdapter.MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.movieTitle.setText(movies[position].title)
        holder.data.setText(movies[position].releaseDate)
        holder.movieDescription.setText(movies[position].overview)
        holder.rating.setText(movies[position].voteAverage.toString())
    }

    override fun getItemCount(): Int = movies.size
}