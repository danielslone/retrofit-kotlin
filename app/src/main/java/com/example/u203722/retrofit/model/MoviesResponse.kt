package com.example.u203722.retrofit.model
import com.google.gson.annotations.SerializedName


/**
 * Created by u203722 on 20/11/17.
 */
data class MoviesResponse (
    val page: Int,
    val results: List<Movie>,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int
)