package com.example.u203722.retrofit.rest
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by u203722 on 20/11/17.
 */
class ApiClient {
    /**
     * used companion for const and static objects
     */
    companion object {
        const val BASE_URL = "http://api.themoviedb.org/3/"
        private var retrofit: Retrofit? = null

        /**
         * created a getter that is Swift-like
         */
        var client: Retrofit? = null
            get() {
                if (Companion.retrofit == null) {
                    Companion.retrofit = Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                }
                return retrofit
            }

//        fun getClient(): Retrofit {
//            if (retrofit == null) {
//                retrofit = Retrofit.Builder()
//                        .baseUrl(BASE_URL)
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build()
//            }
//            return retrofit!! //<--- don't like this because force unwrapping is bad but the check above should also ensure its never null
//        }
    }
}